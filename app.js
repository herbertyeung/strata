'use strict';

var app = require('app');
var locals = {/* ...*/};
var j = require('electron-jade')({pretty: true}, locals);
var BrowserWindow = require('browser-window');

app.on('ready', function(argument) {
  var mainWindow = new BrowserWindow({
    width: 480,
    height: 480
  });

  mainWindow.setTitle('Strata Calc');
  mainWindow.loadURL('file://' + __dirname + '/views/search.jade');
  mainWindow.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
});

app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform != 'darwin') {
    app.quit();
  }
});

