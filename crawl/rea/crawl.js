var page = require('webpage').create();
var system = require('system');

var location;

if (system.args.length < 2) {
  console.log('Usage: crawl.js location')
  console.log('Example: crawl.js "Strathfield, NSW 2135"')
  phantom.exit();
} else {
  location = system.args[1].toLowerCase();

  // https://github.com/ariya/phantomjs/blob/master/examples/direction.js
  // It's in the form: http://www.realestate.com.au/buy/in-strathfield%2c+nsw+2135/list-3
  // http://www.realestate.com.au/buy/property-unit+apartment-in-strathfield%2c+nsw+2135/list-1?source=location-search
  // Or: http://www.realestate.com.au/buy/in-strathfield%2c%20nsw%202135/list-3
  page.open(encodeURI('http://www.realestate.com.au/buy/in-' + location), function(status) {
    if (status !== 'success') {
    } else {
      page.render(location.split(',')[0] + '.png'))

      page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js", function() {
        page.evaluate(function() {
          $("button").click();
        });

        phantom.exit();
      });
    }
  });
}