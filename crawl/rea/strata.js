#!/usr/bin/env node

var fs = require('fs');
var cheerio = require('cheerio');
var json2csv = require('json2csv');
var Bluebird = require('bluebird');
var phantom = require('x-ray-phantom');
var Nightmare = require('nightmare');
var vo = require('vo');
var Xray = require('x-ray');
var xray = Xray();

var REA_URI = 'http://www.realestate.com.au/';
var PROPERTY_TYPE = '/property-unit+apartment-in-';
var OUT_FILE = 'output.csv';

var fields = [
  'source',
  'agency',
  'id',
  'number of apartments',
  'unit number',
  'street',
  'suburb',
  'state',
  'post code',
  'bedrooms',
  'bathrooms',
  'garage spaces',
  'balcony',
  'terrace',
  'swimming pool',
  'gym',
  'sauna',
  'spa',
  'tennis',
  'secure parking',
  'lockup garage',
  'lift',
  'air conditioning',
  'concierge',
  'onsite manager',
  'rooftop',
  'level 1',
  'level 2',
  'level 3',
  'strata',
  'council rates',
  'water rates',
  'internal size (sqm)',
  'size (sqm)',
  'estimated price',
  'date sold'
];

// Base on args we pull the body of the url
// For REA the details are in #primaryContent
// http://www.realestate.com.au/sold/in-Strathfield,%20NSW%202135/list-1
// http://www.realestate.com.au/buy/property-unit+apartment-in-strathfield%2c+nsw+2135/list-1?source=location-search
// http://www.realestate.com.au/property-apartment-nsw-strathfield-121264638
// http://www.realestate.com.au/property-apartment-nsw-strathfield-121455930
// http://www.realestate.com.au/property-apartment-nsw-strathfield-120669357
// http://www.realestate.com.au/property-apartment-nsw-strathfield-121344330
exports.extractListingPages = function (suburb, type, callback) {
  if (suburb === '') {
    suburb = 'Strathfield, NSW 2135';
  }
  var reaUrl = REA_URI + type + PROPERTY_TYPE;
  reaUrl += encodeURIComponent(suburb.toLowerCase());
  reaUrl += '/list-1';
  var propertyUrls = [];
  // xray(reaUrl, '#searchResultsTbl@html')(function(err, content) {
  xray(reaUrl, '#results@html')(function(err, content) {
    if (!err && content) {
      $ = cheerio.load(content);

      // Calculate number of page listings to do
      // Ignore 1st one as we already have list-1
      var numPages = $('.linkList.horizontalLinkList.pagination li').length / 2 - 2;
      // localStorage.setItem('numPages', numPages.toString());
      callback(null, parseInt(numPages));
    } else {
      console.log(err);
      callback(err, null);
    }
  });
};

exports.extractListings = function (page, suburb, type, callback) {
  if (suburb === '') {
    suburb = 'Strathfield, NSW 2135';
  }
  var reaUrl = REA_URI + type + PROPERTY_TYPE;
  reaUrl += encodeURIComponent(suburb.toLowerCase());
  reaUrl += '/list-' + page;
  var propertyUrls = [];
  xray(reaUrl, '#searchResults@html')(function(err, content) {
    if (!err && content) {
      $ = cheerio.load(content);

      // Only grab listings if the suburb matches search
      var pageSuburb = $('.suburb-profile-name')[0].children[0].data;
      if (suburb.toLowerCase().indexOf(pageSuburb) >= 0) {
        // Grab Listings for each page
        var numProperties = $('a.detailsButton').length;
        $('a.detailsButton').each(function(i, elem) {
          propertyUrls.push(elem.attribs.href);
          if (i === numProperties - 1) {
            callback(null, propertyUrls);
          }
        });
      } else {
        callback(null, propertyUrls);
      }
    } else {
      console.log(err);
      callback(err, null);
    }
  });
};

exports.extract = function (reaUrl, suburb, filePath, callback) {
  var nightmare = Nightmare({ show: false })
  .useragent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');

  if (suburb === '') {
    suburb = 'Strathfield, NSW 2135';
  }

  // x(reaUrl, '#primaryContent@html')
  // Check if the page actually has the selector 1st
  function *hasMoreText(url) {
    return yield nightmare
    .goto(url)
    .exists('a.more.interesting');
  }

  function *getReaInfo(moreInfo) {
    var pg;
    if (moreInfo) {
      pg = yield nightmare
      .click('a.more.interesting')
      .wait('a.less.interesting')
      .evaluate(function() {
        return document.documentElement.innerHTML;
      });
    } else {
      pg = yield nightmare
      .evaluate(function() {
        return document.documentElement.innerHTML;
      });
    }

    yield nightmare.end();
    return pg;
  }

  vo(hasMoreText, getReaInfo)(reaUrl, function (err, result) {
    if (!err) {
      $ = cheerio.load(result);
      // Check if it's a project site - if so bail
      if ($('#projectInfo').length > 0) {
        callback(null, true);
      } else {
        // Grab General Features/Outdoor Features
        var agencyName = null;
        var propertyId = null;
        var unitNumber = 0;
        var addressStreet = null;
        var addressLocale = null;
        var addressState = null;
        var addressPostCode = null;
        var bedrooms = 0;
        var bathrooms = 0;
        var carspaces = 0;
        var balcony = false;
        var terrace = false;
        var swimming = false;
        var gym = false;
        var spa = false;
        var sauna = false;
        var tennis = false;
        var secureParking = false;
        var lockupGarage = false;
        var lift = false;
        var airCon = false;
        var concierge = false;
        var onsiteManager = false;
        var roofTop = false;
        var level1 = 0;
        var level2 = 0;
        var level3 = 0;
        var councilAmount = 0;
        var strataAmount = 0;
        var waterAmount = 0;
        var size = 0.0;
        var internalSize = 0.0;
        var soldPrice = 0;
        var dateSold = null;

        new Promise(function(resolve, reject) {
          // Grab Address details
          var len = $('[itemprop="address"]').find('span').length;
          $('[itemprop="address"]').find('span').each(function(i, elem) {
            if (elem.attribs.itemprop === 'streetAddress') {
              if (elem.children[0]) {
                if (elem.children[0].hasOwnProperty('data')) {
                  var splitStreet = elem.children[0].data.split(' ');
                  var buildingVal = splitStreet[0].split('/');
                  unitNumber = parseInt(buildingVal[0].match(/\d+/)[0]);
                  addressStreet = buildingVal[1];
                  for (var j = 1; j <= splitStreet.length - 1; j++) {
                    addressStreet += ' ' + splitStreet[j];
                  }
                }
              }
            } else if (elem.attribs.itemprop === 'addressLocality') {
              if (elem.children[0]) {
                if (elem.children[0].hasOwnProperty('data')) {
                  addressLocale = elem.children[0].data;
                }
              }
            } else if (elem.attribs.itemprop === 'addressRegion') {
              if (elem.children[0]) {
                if (elem.children[0].hasOwnProperty('data')) {
                  addressState = elem.children[0].data;
                }
              }
            } else if (elem.attribs.itemprop === 'postalCode') {
              if (elem.children[0]) {
                if (elem.children[0].hasOwnProperty('data')) {
                  addressPostCode = parseInt(elem.children[0].data);
                }
              }
            }

            if (i === len - 1) {
              // Double check that the suburb is actually correct using postCode
              if (addressPostCode) {
                if (suburb.toLowerCase().indexOf(addressPostCode.toString()) >= 0) {
                  resolve();
                } else {
                  reject(new Error().stack);
                }
              } else {
                reject(new Error().stack);
              }
            }
          });
        })
        .then(function() {
          var featureListLength = $('#features .featureList').find('li').length;
          $('#features .featureList').find('li').each(function(i, elem) {
            // It should be 2 as we have the feature and the value
            var tags = elem.children;
            if (tags.length === 2) {
              if (tags[0].data === 'Property Type:') { // Check for apartment?!?
              } else if (tags[0].data === 'Bedrooms:') { // Grab value
                if (tags[1].name === 'span') {
                  bedrooms = parseInt(tags[1].children[0].data);
                }
              } else if (tags[0].data === 'Bathrooms:') { // Grab value
                if (tags[1].name === 'span') {
                  bathrooms = parseInt(tags[1].children[0].data);
                }
              } else if (tags[0].data.match(/(Garage|Car)\s*\W*(port)*\s*\W*(Space)*/i)) {
                if (tags[1].name === 'span') {
                  carspaces = parseInt(tags[1].children[0].data);
                }
              } else if (tags[0].data === 'Building Size:') {
                if (tags[1].name === 'span') {
                  size = parseFloat(tags[1].children[0].data);
                }
              } else if (tags[0].data === 'Land Size:') {
                if (tags[1].name === 'span') {
                  size = parseFloat(tags[1].children[0].data);
                }
              }
            } else if (tags.length === 1) {
              if (tags[0].data.match(/balcon(y|ies)/i)) {
                balcony = true;
              }

              if (tags[0].data.match(/(swimming|pool)/i)) {
                swimming = true;
              }

              if (tags[0].data.match(/secure/i)) {
                secureParking = true;
              }

              if (tags[0].data.match(/terrace/i)) {
                terrace = true;
              }

              if (tags[0].data.match(/(lift|elevator)/i)) {
                lift = true;
              }

              if (tags[0].data.match(/concierge/i)) {
                concierge = true;
              }

              if (tags[0].data.match(/(manager|management)/i)) {
                onsiteManager = true;
              }

              if (tags[0].data.match(/air\s*\W*\s*con/i)) {
                airCon = true;
              }

              if (tags[0].data.match(/roof/i)) {
                roofTop = true;
              }

              if (tags[0].data.match(/tennis/i)) {
                tennis = true;
              }
            }

            if (i === featureListLength - 1) {
              return new Promise(function(resolve, reject) {
                resolve();
              });
            }
          });
        })
        .then(function() {
          // Grab Property #
          propertyId = $('.property_id')[0].children[0].data.split('Property No. ')[1];
          return new Promise(function(resolve, reject) {
            resolve();
          });
        })
        .then(function() {
          // Grab Agency Name
          agencyName = $('.agencyName')[0].children[0].data;
          if (!agencyName) {
            agencyName = null;
          }
          return new Promise(function(resolve, reject) {
            resolve();
          });
        })
        .then(function() {
          // Grab Sold Price - if available
          if ($('.pricePrefix').length > 0) {
            soldPrice = $('.pricePrefix').next().text();
            dateSold = $('.sold_date').text().replace('Sold Date: ', '');
          }

          return new Promise(function(resolve, reject) {
            resolve();
          });
        })
        .then(function() {
          // Grab details from body text
          var propertyDetails = $('#description p.body')[0].children;

          for (var j = propertyDetails.length - 1; j >= 0; j--) {
            if (propertyDetails[j].type === 'text') {
              var propertyData = propertyDetails[j].data;

              if (!propertyData) {
                return new Promise(function(resolve, reject) {
                  reject(new Error().stack);
                });
              } else {
                // Formats: "Outgoings- Strata : $573/ Council : $218/ Water : $180 per quarterly"
                // "Strata Levies: $1303.05 per quarter approx"
                // "Strata Rates: $1051 p/a "
                var councilRates = /council\W[a-z]*[:=]*\s*\$\d*[.]*\d*/gi;
                var strataRates = /strata\W[a-z]*[:=]*\s*\w*\W*\$\s*\d*[,.]*\d*[.]*\d*/gi;
                var maintenanceRates = /(strata|maintenance)\X*\$\d*[.]*\d*/gi;
                var waterRates = /water\W[a-z]*[:=]*\s*\$\d*[.]*\d*/gi;
                var councilDataMatches = propertyData.match(councilRates);
                var strataDataMatches = propertyData.match(strataRates);
                var maintenanceFeeMatches = propertyData.match(maintenanceRates);
                var waterDataMatches = propertyData.match(waterRates);
                var dollarAmount = /\$\W*\d*[,]*\d*[.]*\d*/;
                if (councilDataMatches) {
                  councilAmount = councilDataMatches[0].match(dollarAmount)[0];
                } else if (strataDataMatches) {
                  strataAmount = strataDataMatches[0].match(dollarAmount)[0];
                  strataAmount = strataAmount.replace(/\W*/, '');
                } else if (waterDataMatches) {
                  waterAmount = waterDataMatches[0].match(dollarAmount)[0];
                } else if (maintenanceFeeMatches) {
                  if (strataAmount === 0) {
                    strataAmount = maintenanceFeeMatches[0].match(dollarAmount)[0];
                    strataAmount = strataAmount.replace(/\W*/, '');
                  }
                }

                // Figure out total and internal sqm
                var internalAreaRegex = /internal\W*(area|size|space)[:=]*\W*\w*\W*\d*\W*(sqm|sq met|square m|m2)/gi;
                // var totalAreaRegex = /(total)*(?<!internal)\W(area|size|spac)[:=]*\W*\w*\W*\d*\W*(sqm|sq met|square m|m2)/gi;
                var totalAreaRegex = /(total|internal)*\W*(area|size|spac)*[:=]*\W*\w*\W*\d*\W*(sqm|sq met|square m|m2)/gi;
                var internalAreaDataMatches = propertyData.match(internalAreaRegex);
                var totalAreaDataMatches = propertyData.match(totalAreaRegex);
                if (internalAreaDataMatches) {
                  var sqmSize = internalAreaDataMatches[0].match(/\d+[.]*\d*\W*(sqm|sq met|square m|m2)/gi)[0];
                  internalSize = parseFloat(sqmSize.match(/\d+[.]*\d*/)[0]);
                }

                if (totalAreaDataMatches) {
                  if (!totalAreaDataMatches[0].match(/internal/gi)) {
                    var sqmSize = totalAreaDataMatches[0].match(/\d+[.]*\d*\W*(sqm|sq met|square m|m2)/gi)[0];
                    size = parseFloat(sqmSize.match(/\d+[.]*\d*/gi)[0]);
                  }
                }

                // Look for facilities that impact on strata
                var swimmingPoolRegex = /(swimming|pool)\s*\W?/gi;
                var gymRegex = /gym(namsium)?\s*\W?/gi;
                var saunaRegex = /sauna\s*\W?/gi;
                var spaRegex = /spa\s*\W?/gi;
                var tennisRegex = /tennis\s*\W?/gi;
                var terraceRegex = /(terrace|patio)\s*\W?/gi;
                var balconyRegex = /balcon(y|ies)\s*\W?/gi;
                var secureParkRegex = /(secure|security)\s*\W?\s*(park|garage)?/gi;
                var lockupGarageRegex = /lock\W*(up)\W?\s*(garage)?/gi;
                var liftRegex = /(lift|elevator)\s*\W?\s*/gi;
                var airConRegex = /air\s*\W?\s*(con)+/gi;
                var conciergeRegex = /concierge\s*\W?/gi;
                var managerRegex = /(manager|management)\s*\W?/gi;
                var roofTopRegex = /roof\s*\W*top/gi;
                var levelRegex = /\d+(nd|th|st|rd)*\s*(and|\&)*\s*\d*(nd|th|st|rd)*\s*(level|floor)/gi;
                var levelRegex2 = /(level|floor)\s*\d+(nd|th|st|rd)*\s*(and|\&)*\s*\d*/gi;
                var levelRegex3 = /(ground|first|second|third|four|fif|six|seven|eight|nin|ten|eleven)+(th)*\s*(and|\&)*\s*\d*(nd|th|st|rd)*\s*(level|floor)/gi;

                var swimmingDataMatches = propertyData.match(swimmingPoolRegex);
                var gymDataMatches = propertyData.match(gymRegex);
                var saunaDataMatches = propertyData.match(saunaRegex);
                var spaDataMatches = propertyData.match(spaRegex);
                var tennisDataMatches = propertyData.match(tennisRegex);
                var terraceDataMatches = propertyData.match(terraceRegex);
                var balconyDataMatches = propertyData.match(balconyRegex);
                var secureDataMatches = propertyData.match(secureParkRegex);
                var lockupGarageDataMatches = propertyData.match(lockupGarageRegex);
                var liftDataMatches = propertyData.match(liftRegex);
                var airConDataMatches = propertyData.match(airConRegex);
                var conciergeDataMatches = propertyData.match(conciergeRegex);
                var managerDataMatches = propertyData.match(managerRegex);
                var roofTopDataMatches = propertyData.match(roofTopRegex);
                var levelDataMatches = propertyData.match(levelRegex);
                var levelData2Matches = propertyData.match(levelRegex2);
                var levelData3Matches = propertyData.match(levelRegex3);

                if (swimmingDataMatches) {
                  swimming = true;
                }

                if (gymDataMatches) {
                  gym = true;
                }

                if (saunaDataMatches) {
                  sauna = true;
                }

                if (spaRegex) {
                  spa = true;
                }

                if (tennisDataMatches) {
                  tennis = true;
                }

                if (terraceDataMatches) {
                  terrace = true;
                }

                if (balconyDataMatches) {
                  balcony = true;
                }

                if (secureDataMatches) {
                  secureParking = true;
                }

                if (lockupGarageDataMatches) {
                  lockupGarage = true;
                }

                if (liftDataMatches) {
                  lift = true;
                }

                if (airConDataMatches) {
                  airCon = true;
                }

                if (conciergeDataMatches) {
                  concierge = true;
                }

                if (managerDataMatches) {
                  onsiteManager = true;
                }

                if (roofTopDataMatches) {
                  roofTop = true;
                }

                if (levelDataMatches) {
                  var levelMatches = levelDataMatches[0].match(/\d+/g);
                  level1 = levelMatches[0];
                  if (levelMatches.length === 2) {
                    level2 = levelMatches[1];
                  } else if (levelMatches.length > 2) {
                    level2 = levelMatches[1];
                    level3 = levelMatches[2];
                  }
                }

                if (levelData2Matches) {
                  var levelMatches = levelData2Matches[0].match(/\d+/g);
                  level1 = levelMatches[0];
                  if (levelMatches.length === 2) {
                    level2 = levelMatches[1];
                  } else if (levelMatches.length > 2) {
                    level2 = levelMatches[1];
                    level3 = levelMatches[2];
                  }
                }

                if (levelData3Matches) {
                  var levelMatches = levelData3Matches[0].match(/(first|second|third)+/g);
                  if (levelData3Matches[0].indexOf('first') >= 0) {
                    level1 = 1;
                  } else if (levelData3Matches[0].indexOf('second') >= 0) {
                    level1 = 2;
                  } else if (levelData3Matches[0].indexOf('third') >= 0) {
                    level1 = 3;
                  } else if (levelData3Matches[0].indexOf('fourth') >= 0) {
                    level1 = 4;
                  } else if (levelData3Matches[0].indexOf('fifth') >= 0) {
                    level1 = 5;
                  } else if (levelData3Matches[0].indexOf('sixth') >= 0) {
                    level1 = 6;
                  } else if (levelData3Matches[0].indexOf('seventh') >= 0) {
                    level1 = 7;
                  } else if (levelData3Matches[0].indexOf('eigth') >= 0) {
                    level1 = 8;
                  } else if (levelData3Matches[0].indexOf('ninth') >= 0) {
                    level1 = 9;
                  } else if (levelData3Matches[0].indexOf('tenth') >= 0) {
                    level1 = 10;
                  } else if (levelData3Matches[0].indexOf('eleventh') >= 0) {
                    level1 = 11;
                  }
                }
              }
            }
          }

          return new Promise(function(resolve, reject) {
            resolve();
          });
        })
        .then(function() {
          // Save to csv file
          var data = [{
            source: reaUrl,
            agency: agencyName,
            id: propertyId,
            'number of apartments': '',
            'unit number': unitNumber,
            street: addressStreet,
            suburb: addressLocale,
            state: addressState,
            'post code': addressPostCode,
            bedrooms: bedrooms,
            bathrooms: bathrooms,
            'garage spaces': carspaces,
            balcony: balcony,
            terrace: terrace,
            'swimming pool': swimming,
            gym: gym,
            sauna: sauna,
            spa: sauna,
            tennis: tennis,
            'secure parking': secureParking,
            'lockup garage': lockupGarage,
            lift: lift,
            'air conditioning': airCon,
            concierge: concierge,
            'onsite manager': onsiteManager,
            'rooftop': roofTop,
            'level 1': level1,
            'level 2': level2,
            'level 3': level3,
            strata: strataAmount,
            'council rates': councilAmount,
            'water rates': waterAmount,
            'internal size (sqm)': internalSize,
            'size (sqm)': size,
            'estimated price': soldPrice,
            'date sold': dateSold
          }];

          var addColumnTitles = false;
          try {
            fs.statSync(filePath);
          } catch(e) {
            addColumnTitles = true;
          }

          json2csv({ data: data, fields: fields, hasCSVColumnTitle: addColumnTitles, eol: '\n' }, function(err, csv) {
            if (err) {
              console.log(err);
            } else {
              if (addColumnTitles) {
                fs.writeFile(filePath, csv, function(err) {
                  if (err) throw err;
                  return new Promise(function(resolve, reject) {
                    resolve();
                  });
                });
              } else {
                fs.appendFile(filePath, csv, function(err) {
                  if (err) throw err;
                  return new Promise(function(resolve, reject) {
                    resolve();
                  });
                });
              }
            }
          });
        })
        .then(function() {
          callback(null, true);
        })
        .catch(function(err) {
          console.log(err);
          console.log(err.stack);
          callback(null, true);
        });
      }
    } else {
      console.log(err);
      callback(null, true);
    }
  });
};
